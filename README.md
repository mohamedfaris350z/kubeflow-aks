# Install Kubeflow 1.6 on Azure AKS

This guide describes how to use kustomize to
deploy Kubeflow on Azure AKS.

## Prerequisites

- Kubernetes compatible on k8s 1.22 [mentioned in kubeflow docs](https://github.com/kubeflow/manifests/tree/v1.6-branch#prerequisites), configured while creating the AKS cluster.

- Install kubectl v1.20.5 [kubectl page](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-kubectl-binary-with-curl-on-linux).

  ```bash
  curl -LO https://dl.k8s.io/release/v1.20.5/bin/linux/amd64/kubectl
  curl -LO "https://dl.k8s.io/v1.20.5/bin/linux/amd64/kubectl.sha256"
  echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
  kubectl version --client
  ```

- Install [kustomize version 3.2.0](https://github.com/kubernetes-sigs/kustomize/releases/tag/v3.2.0)
  ```bash
  curl -sLO https://github.com/kubernetes-sigs/kustomize/releases/download/v3.2.0/kustomize_3.2.0_linux_amd64
  sudo mv kustomize_3.2.0_linux_amd64 /usr/local/bin/kustomize
  sudo chmod +x /usr/local/bin/kustomize
  ```
- Check installed version `kustomize version`

  ```
  kustomize version
  ```

- Install and configure the [Azure Command Line Interface (Az)](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)

  ```bash
  curl -L https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main"
  sudo apt-get update
  sudo apt-get install azure-cli
  az --version
  ```

- (Optional) Install Docker
  ```bash
  sudo apt update
  sudo apt install docker.io -y
  ```

You do not need to have an existing Azure Resource Group or Cluster for AKS (Azure Kubernetes Service). You can create a cluster in the deployment process.

## Azure setup

### Azure Login

To log into Azure from the command line interface, run the following commands

```
az login
az account set --subscription <NAME OR ID OF SUBSCRIPTION>
```

### Initial cluster setup for new cluster

Create a resource group:

```
az group create -n <RESOURCE_GROUP_NAME> -l <LOCATION>
```

Example:

```
az group create -n kf-rg -l eastus
```

Create a specifically defined cluster:

```
az aks create -g <RESOURCE_GROUP_NAME> -n <NAME> -s <AGENT_SIZE> -c <AGENT_COUNT> -l <LOCATION> --generate-ssh-keys
```

**NOTE**: Make sure the VM size is x64 architecture and not ARM, because some pods during deployment have problems with ARM architecture nodes.

Example:

```
az aks create -g kf-rg -n kf-cluster -s Standard_D4s_v3 -c 1 -l eastus --generate-ssh-keys
```

**NOTE**: If you are using a GPU based AKS cluster (For example: AGENT_SIZE=Standard_NC6), you also need to [install the NVidia drivers](https://docs.microsoft.com/azure/aks/gpu-cluster#install-nvidia-drivers) on the cluster nodes before you can use GPUs with Kubeflow.

## Kubeflow installation

After creating resource group and AKS cluster, run the following commands

1. Create user credentials. You only need to run this command once, it will update kubeconfig file to current cluster that you have setup .

   ```
   az aks get-credentials --resource-group <RESOURCE_GROUP_NAME> --name <CLUSTER_NAME> --admin
   ```

   Example:

   ```
   az aks get-credentials --name kf-cluster --resource-group kf-rg --admin
   ```

2. Check kubectl version and that it is connected to the cluster

   ```
   kubectl version
   ```

3. Run the following commands to get kubeflow v1.6 manifest.

   ```bash
   wget https://github.com/kubeflow/manifests/archive/refs/tags/v1.6.1.tar.gz
   # untar the ball
   tar -xvf v1.6.1.tar.gz
   # cd into the manifest directory
   cd manifests-1.6.1
   ```

4. In manifest repo navigate to : `common/istio-1-14/istio-install/base/install.yaml`. Go to this block of code, it should start at line 2900:

   ```yaml
   apiVersion: admissionregistration.k8s.io/v1
   kind: MutatingWebhookConfiguration
   metadata:
     name: istio-sidecar-injector
     labels:
       istio.io/rev: default
       install.operator.istio.io/owning-resource: unknown
       operator.istio.io/component: "Pilot"
       app: sidecar-injector
       release: istio
     ....
   ```

   And, add `annotation:  admissions.enforcer/disabled: "true"` as follows:

   ```yaml
   apiVersion: admissionregistration.k8s.io/v1beta1
   kind: MutatingWebhookConfiguration
   metadata:
     name: istio-sidecar-injector
     annotations:  #here
       admissions.enforcer/disabled: "true"  #here
     labels:
       istio.io/rev: default
       install.operator.istio.io/owning-resource: unknown
       operator.istio.io/component: "Pilot"
       app: sidecar-injector
       release: istio
     ....
   ```

   Save your changes to the yaml file and, now we can install Kubeflow.

   While in the manifests-1.6.1 directory as mentioned above, run the following command to deploy kubeflow.

   ```bash
   while ! kustomize build example | kubectl apply -f -; do echo "Retrying to apply resources"; sleep 10; done
   ```

5. Run this command to check that the resources have been deployed correctly in namespace `kubeflow`:

   ```bash
   watch kubectl get all -n kubeflow
   ```

6. To watch only pods while being deployed, run the following command:

   ```
   watch kubectl get pod --all-namespaces
   ```

7. Expose load balancer

   To expose Kubeflow with a load balancer service, change the type of the istio-ingressgateway service to LoadBalancer by entering this command.

   ```
   kubectl patch service -n istio-system istio-ingressgateway -p '{"spec": {"type": "LoadBalancer"}}'
   ```

   After that, obtain the LoadBalancer IP address by this command (this may take more than 15 minutes)

   ```
   kubectl get svc -n istio-system istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0]}'
   ```

   Running it should return you with a response containing loadbalancer IP where Kubeflow is running.

   Create a self-signed certificate with cert-manager:

   ```
   nano certificate.yaml
   ```

   Enter the following in the `certificate.yaml` file and update the Load-Balancer IP with the one produced in the previous step.

   ```yaml
   apiVersion: cert-manager.io/v1alpha2
   kind: Certificate
   metadata:
     name: istio-ingressgateway-certs
     namespace: istio-system
   spec:
     commonName: istio-ingressgateway.istio-system.svc
     # Use ipAddresses if your LoadBalancer issues an IP address
     ipAddresses:
       - <LoadBalancer IP> # here
     isCA: true
     issuerRef:
       kind: ClusterIssuer
       name: kubeflow-self-signing-issuer
     secretName: istio-ingressgateway-certs
   ```

   Save the file and apply this command to create the certificate:

   ```
   kubectl apply -f certificate.yaml -n istio-system
   ```

8. Steps to open the Kubeflow Dashboard

   Run the following command to get dashboard IP:

   ```
   kubectl get svc -n istio-system istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0]}'
   ```

   Next, open the produced `<loadbalancerip>` in your browser. Login to Kubeflow using default `email : user@example.com` and `password : 12341234`

   **To be Added**:

   - Using SSL certificate
   - Adding step by step commands to use GPU cluster

   **Special thank to original guide made by [ajinkya933](https://github.com/ajinkya933/Kubeflow-on-Azure/tree/main/Kubeflow-1.6-on-Azure)**
